//
//  BoardController.swift
//  hangman-game
//
//  Created by Zajaczkowska Aleksandra and Sagadyn Norbert
//  Copyright © 2020. All rights reserved.
//

import UIKit

class BoardController: UIViewController {
    
    let passwordPool = [
        "bez pracy nie ma kołaczy",
        "jak kuba bogu, tak bóg kubie",
        "apetyt rośnie w miarę jedzenia",
        "baba swoje czart swoje",
        "baba z wozu koniom lżej",
        "bez zachodu nie ma miodu",
        "broda mędrcem nie czyni",
        "cel uświęca środki",
        "chleb i woda nie ma głoda",
        "cicha woda brzegi rwie",
        "co dwie głowy to nie jedna",
        "co nagle to po diable",
        "co kraj to obyczaj",
        "co z oczu to z serca",
        "cudza praca nie wzbogaca",
        "co za dużo to nie zdrowo",
        "ćwiczenie czyni mistrza",
        "czas leczy rany",
        "czas to pieniądz",
        "czwarty pas karty w tas",
        "czym skorupka za młodu",
        "darowanemu koniowi w zęby się nie zagląda",
        "diabeł tkwi w szczegółach",
        "dla chcącego nic trudnego",
        "gdy trwoga to do boga",
        "do trzech razy sztuka",
        "dobra żona mężowi korona",
        "dobry żart tynfa wart",
        "dzieci i ryby głosu nie mają",
        "dzisiaj bal, jutro żal",
        "elektryka prąd nie tyka",
        "fortuna kołem się toczy",
        "gdy kota nie ma myszy harcują",
        "gdzie drwa robią tam wióry lecą",
        "gdzie rzym gdzie krym",
        "głodnemu chleb na myśli",
        "głuchemu próżne słowa",
        "głową muru nie przebijesz",
        "głodny głodnemu wypomni",
        "gość w dom bóg w dom",
        "głupich nie sieją",
        "hulaj dusza piekła nie ma",
        "i króla robaki zjedzą",
        "i wilk syty i owca cała",
        "idzie luty podkuj buty",
        "jak cię widzą tak cię piszą",
        "jaka matka taka córka",
        "jaka praca taka płaca",
        "jaki pan taki kram",
        "jedna jaskółka wiosny nie czyni",
        "kłamstwo ma krótkie nogi",
        "komu w drogę temu czas",
        "kradzione nie tuczy",
        "kropla drąży skałę",
        "kto pierwszy ten lepszy",
        "kto pyta nie błądzi",
        "kto się lubi ten się czubi",
        "kto szuka ten znajdzie",
        "kto z kim przystaje taki się staje",
        "kuć żelazo póki gorące",
        "lepiej późno niż wcale",
        "lepszy rydz niż nic",
        "licho nie śpi",
        "mądry polak po szkodzie",
        "miłość jest ślepa",
        "na bezrybiu i rak ryba",
        "nadzieja matką głupich",
        "najciemniej pod latarnią",
        "natura ciągnie wilka do lasu",
        "nie dla psa kiełbasa",
        "nie ma dymu bez ognia",
        "nie ma róży bez kolców",
        "nie rób drugiemu co tobie nie miłe",
        "nie samym chlebem człowiek żyje",
        "nie szata zdobi człowieka",
        "nie taki diabeł straszny",
        "nie wywołuj wilka z lasu",
        "niedaleko jabłko od jabłoni pada",
        "nowy rok jaki cały rok taki",
        "od przybytku głowa nie boli",
        "okazja czyni złodzieja",
        "oko za oko ząb za ząb",
        "po urbanie lato nastanie",
        "powiedziałeś a powiedz b",
        "powtarzanie jest matką wiedzy",
        "praca tuczy bieda uczy",
        "praca wzbogaca",
        "prawda w oczy kole",
        "raz na wozie raz pod wozem",
        "robota nie zając nie ucieknie",
        "ryba psuje się od głowy",
        "ściany mają uszy",
        "śmiech to zdrowie",
        "śpiesz się powoli",
        "szewc bez butów chodzi",
        "tego kwiatu jest pół światu",
        "wilk syty owca cała",
        "wyszło szydło z worka",
        "żadna praca nie hańbi",
        "złego diabli nie wezmą"
    ];
    
    lazy var randomNumber: Int = Int.random(in: 0 ..< passwordPool.count)
    lazy var word: String = passwordPool[randomNumber]
    lazy var wordLength: Int = word.count
    lazy var hiddenWord: String = ""
    
    var misguided: Int = 0
    let hitLetter: Bool = false;
    
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var resetButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(word)
        
        for i in word {
            if (i == " ") {
                hiddenWord = hiddenWord + " "
            }
            else {
                hiddenWord = hiddenWord + "-"
            }
        }
        
        self.passwordLabel.text = hiddenWord
        
    }
    
    lazy var letter: Character = " "
    lazy var usedLetterArray: Array<Character> = []
    
    func replace(myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString)
        chars[index] = newChar
        let modifiedString = String(chars)
        return modifiedString
    }
    
    func checkLetter(ch: Character) {
        
        letter = ch
        var counter: Int = 0;
        var hit: Bool = false;

        for i in word {
            if (i == letter) {
                hiddenWord = replace(myString: hiddenWord, counter, letter)
                hit = true;
            }
            counter += 1
        }
        
        self.passwordLabel.text = hiddenWord
        
        let uniqueUsedLetterArray = Array(Set(usedLetterArray))
        
        if (hit == false && !uniqueUsedLetterArray.contains(letter)) {
            misguided += 1
            usedLetterArray.append(letter)
        }

        checkResult()
        
    }
    
    func checkResult() {
        
        if (hiddenWord == word) {
            self.passwordLabel.text = "Gratulacje! Hasło zostało odgadnięte!"
            self.imageView.image = UIImage(named: "img.jpg")
            self.resetButton.setImage(UIImage(named: "img10.jpg"), for: .normal)
        }
        else if(misguided == 1) {
            self.imageView.image = UIImage(named: "img1.jpg")
        }
        else if(misguided == 2) {
            self.imageView.image = UIImage(named: "img2.jpg")
        }
        else if(misguided == 3) {
            self.imageView.image = UIImage(named: "img3.jpg")
        }
        else if(misguided == 4) {
            self.imageView.image = UIImage(named: "img4.jpg")
        }
        else if(misguided == 5) {
            self.imageView.image = UIImage(named: "img5.jpg")
        }
        else if(misguided == 6) {
            self.imageView.image = UIImage(named: "img6.jpg")
        }
        else if(misguided == 7) {
            self.imageView.image = UIImage(named: "img7.jpg")
        }
        else if(misguided == 8) {
            self.imageView.image = UIImage(named: "img8.jpg")
        }
        else if (misguided >= 9) {
            self.imageView.image = UIImage(named: "img.jpg")
            self.resetButton.setImage(UIImage(named: "img10.jpg"), for: .normal)
            self.passwordLabel.text = "Nie udało się. Prawidłowe hasło to: " + word
        }
    }
    
    @IBOutlet weak var a: UIButton!
    @IBAction func pressA(_ sender: UIButton) {
        checkLetter(ch: "a")
        a.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishA: UIButton!
    @IBAction func pressPolishA(_ sender: UIButton) {
        checkLetter(ch: "ą")
        polishA.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var b: UIButton!
    @IBAction func pressB(_ sender: UIButton) {
        checkLetter(ch: "b")
        b.setTitleColor(UIColor.gray, for: .normal)
    }

    @IBOutlet weak var c: UIButton!
    @IBAction func pressC(_ sender: UIButton) {
        checkLetter(ch: "c")
        c.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishC: UIButton!
    @IBAction func pressPolishC(_ sender: UIButton) {
        checkLetter(ch: "ć")
        polishC.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var d: UIButton!
    @IBAction func pressD(_ sender: UIButton) {
        checkLetter(ch: "d")
        d.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var e: UIButton!
    @IBAction func pressE(_ sender: UIButton) {
        checkLetter(ch: "e")
        e.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishE: UIButton!
    @IBAction func pressPolishE(_ sender: UIButton) {
        checkLetter(ch: "ę")
        polishE.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var f: UIButton!
    @IBAction func pressF(_ sender: UIButton) {
        checkLetter(ch: "f")
        f.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var g: UIButton!
    @IBAction func pressG(_ sender: UIButton) {
        checkLetter(ch: "g")
        g.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var h: UIButton!
    @IBAction func pressH(_ sender: UIButton) {
        checkLetter(ch: "h")
        h.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var i: UIButton!
    @IBAction func pressI(_ sender: UIButton) {
        checkLetter(ch: "i")
        i.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var j: UIButton!
    @IBAction func pressJ(_ sender: UIButton) {
        checkLetter(ch: "j")
        j.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var k: UIButton!
    @IBAction func pressK(_ sender: UIButton) {
        checkLetter(ch: "k")
        k.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var l: UIButton!
    @IBAction func pressL(_ sender: UIButton) {
        checkLetter(ch: "l")
        l.setTitleColor(UIColor.gray, for: .normal)
    }

    @IBOutlet weak var polishL: UIButton!
    @IBAction func pressPolishL(_ sender: UIButton) {
        checkLetter(ch: "ł")
        polishL.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var m: UIButton!
    @IBAction func pressM(_ sender: UIButton) {
        checkLetter(ch: "m")
        m.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var n: UIButton!
    @IBAction func pressN(_ sender: UIButton) {
        checkLetter(ch: "n")
        n.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishN: UIButton!
    @IBAction func pressPolishN(_ sender: UIButton) {
        checkLetter(ch: "ń")
        polishN.setTitleColor(UIColor.gray, for: .normal)
    }

    @IBOutlet weak var o: UIButton!
    @IBAction func pressO(_ sender: UIButton) {
        checkLetter(ch: "o")
        o.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishO: UIButton!
    @IBAction func pressPolishO(_ sender: UIButton) {
        checkLetter(ch: "ó")
        polishO.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var p: UIButton!
    @IBAction func pressP(_ sender: UIButton) {
        checkLetter(ch: "p")
        p.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var q: UIButton!
    @IBAction func pressQ(_ sender: UIButton) {
        checkLetter(ch: "q")
        q.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var r: UIButton!
    @IBAction func pressR(_ sender: UIButton) {
        checkLetter(ch: "r")
        r.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var s: UIButton!
    @IBAction func pressS(_ sender: UIButton) {
        checkLetter(ch: "s")
        s.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishS: UIButton!
    @IBAction func pressPolishS(_ sender: UIButton) {
        checkLetter(ch: "ś")
        polishS.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var t: UIButton!
    @IBAction func pressT(_ sender: UIButton) {
        checkLetter(ch: "t")
        t.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var u: UIButton!
    @IBAction func pressU(_ sender: UIButton) {
        checkLetter(ch: "u")
        u.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var v: UIButton!
    @IBAction func pressV(_ sender: UIButton) {
        checkLetter(ch: "v")
        v.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var w: UIButton!
    @IBAction func pressW(_ sender: UIButton) {
        checkLetter(ch: "w")
        w.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var x: UIButton!
    @IBAction func pressX(_ sender: UIButton) {
        checkLetter(ch: "x")
        x.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var y: UIButton!
    @IBAction func pressY(_ sender: UIButton) {
        checkLetter(ch: "y")
        y.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var z: UIButton!
    @IBAction func pressZ(_ sender: UIButton) {
        checkLetter(ch: "z")
        z.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishZ1: UIButton!
    @IBAction func pressPolishZ1(_ sender: UIButton) {
        checkLetter(ch: "ż")
        polishZ1.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet weak var polishZ2: UIButton!
    @IBAction func pressPolishZ2(_ sender: UIButton) {
        checkLetter(ch: "ź")
        polishZ2.setTitleColor(UIColor.gray, for: .normal)
    }
    
    @IBOutlet var buttons: [UIButton]!
    
    @IBAction func resetGame(_ sender: Any) {
        
        for button in self.buttons {
            button.setTitleColor(UIColor.white, for: .normal)
        }
        
        randomNumber = Int.random(in: 0 ..< passwordPool.count)
        word = passwordPool[randomNumber]
        
        misguided = 0
        hiddenWord = ""
        
        self.viewDidLoad()
        self.imageView.image = UIImage(named: "img0.jpg")
        self.resetButton.setImage(UIImage(named: "img.jpg"), for: .normal)
        
    }
}
